import 'dart:convert';
import 'dart:core';
import 'package:flutter_dotenv/flutter_dotenv.dart';
import 'package:http/http.dart' as http;
import 'package:flutter/material.dart';


final uriWeather = "http://api.openweathermap.org/data/2.5/forecast?id=524901&appid=${dotenv.env['METEO_API_KEY']!}"; 

class Meteo extends StatelessWidget {
  const Meteo({super.key, required this.title});

  final String title;

  @override
  Widget build(BuildContext context) {
    return const Placeholder();
  }
}

void fetchData(link) async {
  final response = await http.get(Uri.parse(link));

    if (response.statusCode == 200) {
      print('Données récupérées avec succès: ${jsonEncode(response.body)}');
    } else {
      print('Échec de la récupération des données: ${response.statusCode}');
    }
}


void fetchNinjaData(String name) async {

  final String url = 'https://api.api-ninjas.com/v1/city?name=$name';
  
  try {
    final response = await http.get(
        Uri.parse(url),
        headers: {'X-Api-Key': dotenv.env['CITY_API_KEY']!},
      );
    
    if (response.statusCode == 200) {
      // Le serveur a renvoyé avec succès les données demandées
      // print('Données récupérées avec succès: ${response.body}');

      final List<dynamic> jsonData = jsonDecode(response.body);

      if (jsonData.isNotEmpty) {
        final Map<String, dynamic> cityData = jsonData[0];
        final double latitude = cityData['latitude'];
        final double longitude = cityData['longitude'];

        print("latitude = $latitude");
        print("longitude = $longitude");

        final String uriWeather2 = "https://api.openweathermap.org/data/2.5/weather?lat=$latitude&lon=$longitude&appid=${dotenv.env['METEO_API_KEY']!}";
        fetchData(uriWeather2);
      }

    } else {
      // Si le serveur a renvoyé une erreur
      print('Échec de la récupération des données: ${response.statusCode}');
      print('Erreur: ${response.body}');
    }
  } catch (e) {
    // En cas d'erreur lors de la connexion à l'API
    print('Erreur lors de la requête HTTP: $e');
  }
}
