import 'package:flutter/material.dart';
import 'package:meteo/meteo.dart';
import 'package:flutter_dotenv/flutter_dotenv.dart';


Future main() async {
  await dotenv.load(fileName: ".env");
  runApp(const MainApp());

  // fetchData(uriWeather);
  // fetchNinjaData("metz");
}

class MainApp extends StatelessWidget {
  const MainApp({super.key});

  @override
  Widget build(BuildContext context) {
    return const MaterialApp(
      title: 'Meteo',

      // home: Meteo(title: 'Meteo'),
      home: MeteoForm(),

    );
  }
}

class MeteoForm extends StatefulWidget {

  const MeteoForm({super.key});

  @override
  // ignore: library_private_types_in_public_api
  _MeteoFormState createState() => _MeteoFormState();
}

class _MeteoFormState extends State<MeteoForm> {
  final TextEditingController _cityController = TextEditingController();

  void _fetchWeatherData() {
    final String cityName = _cityController.text;
    fetchNinjaData(cityName);
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text('Meteo'),
      ),
      body: Padding(
        padding: const EdgeInsets.all(20.0),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            TextField(
              controller: _cityController,
              decoration: const InputDecoration(labelText: 'Nom de la ville'),
            ),
            const SizedBox(height: 20),
            ElevatedButton(
              onPressed: _fetchWeatherData,
              child: const Text('Obtenir la météo'),
            ),
          ],
        ),
      ),
    );
  }
}